#!/bin/bash
# Start cat videos

DIR=/mnt/data
VIDEOS=$DIR/videos
PICKS=$DIR/picks
SERVICE=vlc
OPTION=3

if pgrep -x "$SERVICE" > /dev/null; then
    echo "$SERVICE is already running!"
else
    if [ -e "$DIR" ]; then
        echo "$DIR exists."

        case $OPTION in

            1)
                echo "Starting $SERVICE..."
                echo "Mode: Randomized, Looped, Forever"
                echo "Folder: $DIR"
                vlc -L -Z -q $VIDEOS &>/dev/null
            ;;
            2)
                echo "Starting $SERVICE..."
                echo "Mode: Looped, Forever"
                echo "Video: mice-black_yellow.mkv"
                vlc -L -q $VIDEOS/mice-black_yellow.mkv &>/dev/null
            ;;
            3)
		echo "Starting $SERVICE..."
		echo "Mode: Looped, Forever"
		echo "Folder: $PICKS"
		vlc -L -Z -q $PICKS &>/dev/null
            ;;
            *)
                echo "Invalid option..."
            ;;
        esac

    else
        echo "$DIR does not exist."
        echo "$SERVICE will not start."
    fi
fi

