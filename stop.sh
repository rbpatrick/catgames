#!/bin/bash
# Stop cat games

SERVICE=vlc
COUNT=0

if pgrep -x "$SERVICE" >/dev/null; then
    echo "Stopping $SERVICE..."
    pkill $SERVICE
fi

